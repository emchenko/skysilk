<?php

session_start();

// load the (optional) Composer auto-loader
if (file_exists('../vendor/autoload.php')) {
    require '../vendor/autoload.php';
}

// load application config (error reporting etc.)
require '../app/config/config.php';

// load application class
require '../src/SkysilkBundle/Core/Application.php';
require '../src/SkysilkBundle/Core/BaseController.php';

// start the application
$app = new Skysilk\Core\Application();

