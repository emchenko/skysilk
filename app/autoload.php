<?php

$loader = require __DIR__ . '/../vendor/autoload.php';
$loader->addPsr4('Skysilk\\', __DIR__);

Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem(
    array (APP_PATH.'/views/')
);
// set up environment
$params = array(
    'cache' => BASE_PATH.'/cache',
    'auto_reload' => true, // disable cache
    'autoescape' => true
);
$this->twig = new Twig_Environment($loader, $params);