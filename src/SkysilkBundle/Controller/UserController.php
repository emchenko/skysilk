<?php

namespace Skysilk\Controller;


use Skysilk\Core\BaseController;
use Skysilk\Core\ErrorHandler;
use Skysilk\Helper\UserHelper;
use Skysilk\Manager\UserManager;
use Skysilk\Model\User;
use Skysilk\Service\TokenGenerator;

class UserController extends BaseController
{
    /**
     * List Action
     */
    public function list()
    {
        if(!$this->getSession()->exists('user')) {
            header('Location: /user/login');
        }

        $userManager = UserManager::getInstance();

        $users = $userManager->loadAll();

        $this->render('SkysilkBundle/userList.html.twig', ['users' => $users]);
    }

    /**
     * @param int $userId
     */
    public function remove($userId)
    {
        if(!$this->getSession()->exists('user')) {
            header('Location: /user/login');
        }

        $userManager = UserManager::getInstance();

        $user = $userManager->load($userId);

        if($user) {
            $userManager->remove($user);
        }

        header('Location: /user/list');
    }

    /**
     * Edit Action
     *
     * @param int $userId
     */
    public function edit(int $userId)
    {
        if(!$this->getSession()->exists('user')) {
            header('Location: /user/login');
        }

        $userManager = UserManager::getInstance();

        $user = $userManager->load($userId);

        $request = $this->getRequest();

        if($request->hasPostRequest()) {

            $requestData = $this->getRequest()->getPostRequest();

            $userHelper = UserHelper::getInstance();

            if($userHelper->validateUpdateData($requestData)) {

                $result = $userManager->update($user, $requestData);

                if($result) {
                    header("Location: /user/list");
                }
            }

        }

        $this->render('SkysilkBundle/userEdit.html.twig', ['user' => $user]);
    }

    /**
     * Login Action
     */
    public function login()
    {
        $request = $this->getRequest();

        if($request->hasPostRequest()) {

            $requestData = $this->getRequest()->getPostRequest();

            $userHelper = UserHelper::getInstance();

            if($userHelper->validateLoginData($requestData)) {
                $result = $userHelper->login($requestData['username'], $requestData['password']);

                if($result) {
                    header("Location: /user/list");
                    die;
                }
            }
        }

        $this->render('SkysilkBundle/login.html.twig');
    }

    public function logout()
    {
        $userHelper = UserHelper::getInstance();

        $result = $userHelper->logout();

        if($result) {
            header("Location: /");
            die;
        }
    }

    /**
     * Register Action
     */
    public function register()
    {
        $request = $this->getRequest();

        if($request->hasPostRequest()) {

            $requestData = $this->getRequest()->getPostRequest();

            $userHelper = UserHelper::getInstance();

            if($userHelper->validateRegisterData($requestData)) {

                $userManager = UserManager::getInstance();
                $tokenGenerator = TokenGenerator::getInstance();
                $token = $tokenGenerator->generateToken();

                $requestData['activationToken'] = $token;

                $user = $userManager->create($requestData);

                if($user && $user instanceof User) {

                    $userHelper->sendActivationEmail($user);

                    header("Location: /user/login");
                    die;
                }
            }
        }

        $this->render('SkysilkBundle/register.html.twig');
    }

    public function activation($token)
    {
        $errorHandler = ErrorHandler::getInstance();

        if(empty($token)) {
            $errorHandler->addError('Token is empty.');
            $this->render('SkysilkBundle/activation.html.twig');
        }

        $userManager = UserManager::getInstance();
        $user = $userManager->loadByToken(urldecode($token));

        if(!$user) {
            $this->render('SkysilkBundle/activation.html.twig');
        }

        $result = $userManager->activate($user);
        $params = [];

        if($result) {
            $params['activationMessage'] = 'Thank You!';
        }

        $this->render('SkysilkBundle/activation.html.twig', $params);
    }
}