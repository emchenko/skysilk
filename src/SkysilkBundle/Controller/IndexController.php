<?php

namespace Skysilk\Controller;


use Skysilk\Core\BaseController;


class IndexController extends BaseController
{
    public function index()
    {
        $this->render('SkysilkBundle/index.html.twig');
    }
}