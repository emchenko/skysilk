<?php

namespace Skysilk\Manager;


use Skysilk\Core\ErrorHandler;
use Skysilk\Model\User;
use Skysilk\Repository\UserRepository;

class UserManager
{
    /**
     * @var UserManager
     */
    private static $instance;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ErrorHandler
     */
    private $errorHandler;

    /**
     * @return UserManager
     */
    public static function getInstance(): UserManager
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * UserManager constructor.
     */
    public function __construct()
    {
        $this->userRepository = UserRepository::getInstance();
        $this->errorHandler = ErrorHandler::getInstance();
    }

    public function loadAll()
    {
        $users = [];

        $result = $this->userRepository->findAll();

        if(count($result)) {
            foreach ($result as $data) {
                $users[] = new User($data);
            }
        }

        return $users;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function load(int $id)
    {
        $result = $this->userRepository->find($id);

        if($result) {
            $user = new User($result);
            return $user;
        }

        $this->errorHandler->addError("User not found.");

        return false;
    }

    /**
     * @param string $username
     * @return mixed
     */
    public function loadByUsername(string $username)
    {
        $result = $this->userRepository->findByUsername($username);

        if($result) {
            $user = new User($result);
            return $user;
        }

        $this->errorHandler->addError("User not found.");

        return false;
    }

    /**
     * @param string $token
     * @return mixed
     */
    public function loadByToken(string $token)
    {
        $result = $this->userRepository->findByToken($token);

        if($result) {
            $user = new User($result);
            return $user;
        }

        $this->errorHandler->addError("User not found.");

        return false;
    }

    /**
     * @param string $username
     * @param string $password
     * @return mixed
     */
    public function loadByUsernameAndPassword(string $username, string $password)
    {
        $result = $this->userRepository->findByUsernameAndPassword($username, $password);

        if($result) {
            $user = new User($result);
            return $user;
        }

        $this->errorHandler->addError("User not found.");

        return false;
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function loadByEmail(string $email)
    {
        $result = $this->userRepository->findByEmail($email);

        if($result) {
            $user = new User($result);
            return $user;
        }

        $this->errorHandler->addError("User not found.");

        return false;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create($data)
    {
        //simple password hash
        $data['password'] = md5($data['password']);

        $user = $this->loadByUsername($data['username']);

        if($user) {
            $this->errorHandler->addError("User with an username {$data['username']} is already registered.");
            return false;
        }

        $user = $this->loadByEmail($data['email']);

        if($user) {
            $this->errorHandler->addError("User with an email {$data['email']} is already registered.");
            return false;
        }

        $result = $this->userRepository->create($data);

        if($result) {
            $user = $this->load($result);
            return $user;
        }

        $this->errorHandler->addError("User was not created.");

        return false;
    }

    /**
     * @param User $user
     * @param array $data
     * @return bool
     */
    public function update(User $user, $data = [])
    {
        if(count($data)) {

            if(isset($data['username'])) {
                $user->setUsername($data['username']);
            }

            if(isset($data['email'])) {
                $user->setEmail($data['email']);
            }

            if(isset($data['password']) && !empty($data['password'])) {
                $user->setPassword(md5($data['password']));
            }
        }

        $result = $this->userRepository->update($user);

        if($result) {
            return true;
        }

        $this->errorHandler->addError("User was not updated.");

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function remove(User $user): bool
    {
        $result = $this->userRepository->remove($user->getId());

        if($result) {
            return true;
        }

        $this->errorHandler->addError("User was not deleted.");
    }

    /**
     * @param User $user
     * @return bool
     */
    public function activate(User $user): bool
    {
        $user->setEnabled(1);
        $user->setActivationToken(null);

        return $this->update($user);
    }
}