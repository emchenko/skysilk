<?php

namespace Skysilk\Core;


class ErrorHandler
{
    /**
     * @var ErrorHandler
     */
    private static $instance;

    /**
     * @return ErrorHandler
     */
    public static function getInstance(): ErrorHandler
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @var array $errors
     */
    private $errors = [];

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param $error
     */
    public function addError($error)
    {
        $this->errors[] = $error;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return !!count($this->errors);
    }
}