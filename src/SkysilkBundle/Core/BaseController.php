<?php

namespace Skysilk\Core;


use Skysilk\Service\Request;
use Skysilk\Service\Session;
use Skysilk\Service\Twig;


class BaseController
{
    /**
     * @var \Twig_Environment
     */
    public $twig = null;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->twig = Twig::getInstance()->getEnvironment();
    }

    /**
     * @param string $templateName
     * @param array $params
     */
    public function render($templateName, $params = [])
    {
        $validatorError = ErrorHandler::getInstance();
        $session = $this->getSession();
        $request = $this->getRequest();

        if($validatorError->hasErrors()) {
            $params['errors'] = $validatorError->getErrors();
        }

        if($session->exists('user')) {

            $params['sessionUser'] = $session->get('user');
        }

        if($request->getPostRequest()) {
            $params['request'] = $request->getPostRequest();
        }

        print $this->twig->render($templateName, $params);
        die;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return Request::getInstance();
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return Session::getInstance();
    }
}