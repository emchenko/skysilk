<?php

namespace Skysilk\Core;


class Application
{
    /**
     * @var null
     */
    private $urlController = null;

    /** @var null The method (of the above controller), often also named "action" */
    private $urlAction = null;

    /** @var array URL parameters */
    private $urlParams = [];

    /**
     * "Start" the application:
     * Analyze the URL elements and calls the according controller/method or the fallback
     */
    public function __construct()
    {
        // create array with URL parts in $url
        $this->splitUrl();

        // check for controller: does such a controller exist ?
        if (file_exists( __DIR__ . '/../Controller/' . $this->urlController . '.php')) {

            $class = '\\Skysilk\\Controller\\' . $this->urlController;
            
            $this->urlController = new $class();

            // check for method: does such a method exist in the controller ?
            if (method_exists($this->urlController, $this->urlAction)) {

                if(!empty($this->urlParams)) {
                    // Call the method and pass arguments to it
                    call_user_func_array(array($this->urlController, $this->urlAction), $this->urlParams);
                } else {
                    // If no parameters are given, just call the method without parameters, like $this->home->method();
                    $this->urlController->{$this->urlAction}($this->urlParams);
                }

            } else {
                // default/fallback: call the index() method of a selected controller
                $this->urlController->index();
            }
        } else {
            $indexController = new \Skysilk\Controller\IndexController();
            $indexController->index();
        }
    }

    /**
     * Get and split the URL
     */
    private function splitUrl()
    {
        if (isset($_SERVER['REQUEST_URI'])) {

            // split URL
            $url = trim($_SERVER['REQUEST_URI'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            // Put URL parts into according properties
            $this->urlController = isset($url[0]) ? ucfirst($url[0]) . 'Controller' : null;
            $this->urlAction = isset($url[1]) ? $url[1] : null;

            // Remove controller and action from the split URL
            unset($url[0], $url[1]);

            // Rebase array keys and store the URL params
            $this->urlParams = array_values($url);

            // for debugging. uncomment this if you have problems with the URL
//            echo 'Controller: ' . $this->urlController . '<br>';
//            echo 'Action: ' . $this->urlAction . '<br>';
//            echo 'Parameters: ' . print_r($this->urlParams, true) . '<br>';
        }
    }
}