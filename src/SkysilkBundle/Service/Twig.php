<?php

namespace Skysilk\Service;


class Twig
{
    /**
     * @var Singleton
     */
    private static $instance;

    /**
     * @var \Twig_Environment
     */
    private $environment;

    /**
     * @return Twig
     */
    public static function getInstance(): Twig
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(
            array (
                "../app/views"
            )
        );
        // set up environment
        $params = array(
            'cache' => CACHE_DIR,
            'auto_reload' => true, // disable cache
            'autoescape' => false
        );

        $this->environment = new \Twig_Environment($loader, $params);
    }

    /**
     * @return \Twig_Environment
     */
    public function getEnvironment()
    {
        return $this->environment;
    }
}