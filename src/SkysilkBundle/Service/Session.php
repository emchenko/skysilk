<?php

namespace Skysilk\Service;


class Session
{
    /**
     * @var Session
     */
    private static $instance;

    /**
     * @return Session
     */
    public static function getInstance(): Session
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function get($name)
    {
        if($this->exists($name)) {
            return $_SESSION[$name];
        }

        throw new \Exception("Session variable {$name} not found.");
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function exists($name): bool
    {
        return isset($_SESSION[$name]);
    }

    /**
     * @param string $name
     */
    public function destroy($name)
    {
        unset($_SESSION[$name]);
    }
}