<?php

namespace Skysilk\Service;


class Request
{
    /**
     * @var Request
     */
    private static $instance;

    /**
     * @return Request
     */
    public static function getInstance(): Request
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @return array
     */
    public function getPostRequest(): array
    {
        return $_POST;
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function getPostRequestParam($name)
    {
        if(isset($_POST[$name])) {
            return $_POST[$name];
        }

        throw new \Exception("Request POST param {$name} not found.");
    }

    /**
     * @return bool
     */
    public function hasPostRequest(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }
}