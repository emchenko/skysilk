<?php

namespace Skysilk\Service;


class DatabaseConnection
{
    /**
     * @var DatabaseConnection
     */
    private static $instance;

    /**
     * @var \PDO
     */
    private $connection;

    /**
     * @return DatabaseConnection
     */
    public static function getInstance(): DatabaseConnection
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Database constructor.
     */
    public function __construct()
    {
        $options = [
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING
        ];

        $this->connection = new \PDO(DB_TYPE . ':host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME, DB_USER, DB_PASS, $options);
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }
}