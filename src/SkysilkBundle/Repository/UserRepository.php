<?php

namespace Skysilk\Repository;


use Skysilk\Service\DatabaseConnection;
use Skysilk\Model\User;

class UserRepository
{
    /**
     * @var UserRepository
     */
    private static $instance;

    /**
     * @return UserRepository
     */
    public static function getInstance(): UserRepository
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @var \PDO
     */
    private $connection;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->connection = DatabaseConnection::getInstance()->getConnection();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $stmt = $this->connection->prepare("SELECT * FROM `user`");

        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        $stmt = $this->connection->prepare("SELECT * FROM `user` WHERE `id` = :id");
        $stmt->bindValue('id', $id);
        $stmt->execute();

        $data = $stmt->fetch();

        return $data;
    }

    /**
     * @param string $username
     * @return mixed
     */
    public function findByUsername(string $username)
    {
        $stmt = $this->connection->prepare("SELECT * FROM `user` WHERE `username` = :username");
        $stmt->bindValue('username', $username);
        $stmt->execute();

        $data = $stmt->fetch();

        return $data;
    }

    /**
     * @param string $token
     * @return mixed
     */
    public function findByToken(string $token)
    {
        $stmt = $this->connection->prepare("SELECT * FROM `user` WHERE `activation_token` = :token");
        $stmt->bindValue('token', $token);
        $stmt->execute();

        $data = $stmt->fetch();

        return $data;
    }

    /**
     * @param string $username
     * @param string $password
     * @return mixed
     */
    public function findByUsernameAndPassword(string $username, string $password)
    {
        $stmt = $this->connection->prepare("SELECT * FROM `user` WHERE `username` = :username AND `password`=:password");
        $stmt->bindValue('username', $username);
        $stmt->bindValue('password', md5($password));
        $stmt->execute();

        $data = $stmt->fetch();

        return $data;
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function findByEmail(string $email)
    {
        $stmt = $this->connection->prepare("SELECT * FROM `user` WHERE `email` = :email");
        $stmt->bindValue('email', $email);
        $stmt->execute();

        $data = $stmt->fetch();

        return $data;
    }

    public function create($data)
    {
        $sql = "INSERT INTO `user` (`username`, `password`, `enabled`, `email`, `activation_token`) 
                VALUES (:username, :password, :enabled, :email, :activationToken)";

        $stmt = $this->connection->prepare($sql);
        $result = $stmt->execute([
                    'username' => $data['username'],
                    'password' => $data['password'],
                    'enabled' => 0,
                    'email' => $data['email'],
                    'activationToken' => $data['activationToken']
        ]);

        if($result) {
            return (int) $this->connection->lastInsertId('id');
        }
    }

    public function update(User $user)
    {
        $sql = "UPDATE `user` 
                SET `username`=:username, `password`=:password, `enabled`=:enabled, `email`=:email, `activation_token`=:activationToken
                WHERE `id`=:id
                ";

        $stmt = $this->connection->prepare($sql);
        return $stmt->execute([
                    'username' => $user->getUsername(),
                    'password' => $user->getPassword(),
                    'enabled' => $user->getEnabled(),
                    'email' => $user->getEmail(),
                    'activationToken' => $user->getActivationToken(),
                    'id' => $user->getId()
                ]);
    }

    public function remove(int $id)
    {
        $sql = "DELETE FROM `user` WHERE `id`=:id";

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('id', $id);

        return $stmt->execute();
    }
}