<?php

namespace Skysilk\Helper;


use Skysilk\Core\ErrorHandler;
use Skysilk\Manager\UserManager;
use Skysilk\Model\User;
use Skysilk\Service\Session;
use Swift_SmtpTransport;

class UserHelper
{
    /**
     * @var UserHelper
     */
    private static $instance;

    /**
     * @return UserHelper
     */
    public static function getInstance(): UserHelper
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validateLoginData($data): bool
    {
        $username = isset($data['username']) ? $data['username'] : null;
        $password = isset($data['password']) ? $data['password'] : null;

        if(empty($username) || empty($password)) {
            $validatorError = ErrorHandler::getInstance();
            $validatorError->addError('Username or Password can not be empty.');

            return false;
        }

        return true;
    }

    /**
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function login(string $username, string $password): bool
    {
        $userManager = UserManager::getInstance();

        $user = $userManager->loadByUsernameAndPassword($username, $password);

        if($user) {

            if(!$user->getEnabled()) {
                $errorHandler = ErrorHandler::getInstance();
                $errorHandler->addError("User {$user->getUsername()} is not activated.");

                return false;
            }

            $session = Session::getInstance();

            $session->set('user', $user);

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function logout(): bool
    {
        $session = Session::getInstance();

        if($session->exists('user')) {
            $session->destroy('user');
        }

        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validateRegisterData(array $data): bool
    {
        $username = isset($data['username']) ? $data['username'] : null;
        $password = isset($data['password']) ? $data['password'] : null;
        $repeatPassword = isset($data['repeatPassword']) ? $data['repeatPassword']: null;
        $email = isset($data['email']) ? $data['email'] : null;

        $validatorError = ErrorHandler::getInstance();


        if(empty($username)) {
            $validatorError->addError('Username is required.');
        }

        if(empty($password)) {
            $validatorError->addError('Password is required.');
        }

        if(empty($repeatPassword)) {
            $validatorError->addError('Repeat password is required.');
        }

        if(empty($email)) {
            $validatorError->addError('Email is required.');
        }

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $validatorError->addError('Email is invalid.');
        }

        if($password !== $repeatPassword) {
            $validatorError->addError('Passwords do not match.');
        }

        if($validatorError->hasErrors()) {
            return false;
        }

        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function validateUpdateData($data)
    {
        $username = isset($data['username']) ? $data['username'] : null;
        $password = isset($data['password']) ? $data['password'] : null;
        $repeatPassword = isset($data['repeatPassword']) ? $data['repeatPassword']: null;
        $email = isset($data['email']) ? $data['email'] : null;

        $validatorError = ErrorHandler::getInstance();

        if(empty($username)) {
            $validatorError->addError('Username is required.');
        }

        if(empty($email)) {
            $validatorError->addError('Email is required.');
        }

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $validatorError->addError('Email is invalid.');
        }

        if(!empty($password)) {

            if ($password !== $repeatPassword) {
                $validatorError->addError('Passwords do not match.');
            }

        }

        if($validatorError->hasErrors()) {
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function sendActivationEmail(User $user): bool
    {

        $transport = new \Swift_SmtpTransport(SMTP_HOST, SMTP_PORT, SMTP_SECURITY);
        $transport->setUsername(SMTP_USERNAME);
        $transport->setPassword(SMTP_PASSWORD);

        $mailer = new \Swift_Mailer($transport);

        $message = new \Swift_Message('Skysilk Actiovation Email');
        $message->setFrom(['emchenkoivan@gmail.com' => 'Ivan Iemchenko']);
        $message->setTo([$user->getEmail() => $user->getUsername()]);

        $link = SITE_URL . '/user/activation/' . $user->getActivationToken();

        $message->setBody("
                    Hi {$user->getUsername()} !\n\n
                    You have been registered on Skysilk test bundle.\n
                    
                    Please follow this link {$link} and finish registration.\n\n
                    
                    Best regards,\n
                    Ivan Iemchenko
                ");

        if (!$mailer->send($message, $errors))
        {
            $errorHandler = ErrorHandler::getInstance();
            $errorHandler->addError(implode(',' ,$errors));

            return false;
        }

        return true;
    }
}